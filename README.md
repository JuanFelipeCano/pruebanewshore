Angular CLI: 15.1.3
Node: 18.13.0
Package Manager: npm 9.3.1
OS: win32 x64

Angular:

Package                      Version
------------------------------------------------------
@angular-devkit/architect    0.1501.3 (cli-only)
@angular-devkit/core         15.1.3 (cli-only)
@angular-devkit/schematics   15.1.3 (cli-only)
@schematics/angular          15.1.3 (cli-only)

Puede ver la solución aquí: https://prueba-newshore.web.app/
