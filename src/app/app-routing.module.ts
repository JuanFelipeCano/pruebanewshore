import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'booking',
    loadChildren: () => import('@features/dashboard/presentation').then(m => m.DashboardModule),
  },
  {
    path: '',
    redirectTo: '/booking',
    pathMatch: 'full',
  },
  {
    path: '**',
    redirectTo: '/booking',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
