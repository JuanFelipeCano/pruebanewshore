import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { BookingRoutingModule } from './booking-routing.module';
import { BookingComponent } from './pages/booking/booking.component';
import { BookingInteractor } from '@features/booking/core';
import { BookingRepository } from '@features/booking/core/repositories';
import { UiModule } from '@ui';
import { JourneyComponent } from './components/journey/journey.component';


@NgModule({
  declarations: [
    BookingComponent,
    JourneyComponent
  ],
  imports: [
    CommonModule,
    BookingRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    UiModule,
  ],
  providers: [
    {
      provide: BookingInteractor,
      useClass: BookingInteractor,
      deps: [BookingRepository]
    }
  ]
})
export class BookingModule { }
