import { Component, Input, OnInit } from '@angular/core';
import { Currency } from '@commons/store';
import { JourneyEntity } from '@features/booking/core/entities';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-journey',
  templateUrl: './journey.component.html',
  styleUrls: ['./journey.component.scss']
})
export class JourneyComponent implements OnInit {

  @Input()
  public journey: JourneyEntity;

  @Input()
  public title: string;

  public currency$: Observable<Currency | any>;
  public currency: Currency;

  constructor(
    private store: Store<{ currency: Currency }>
  ) {
    this.currency$ = this.store.select('currency');
  }

  public ngOnInit(): void {
    this.getCurrency();
  }

  private getCurrency() {
    this.currency$.subscribe(item => this.currency = item.currency);
  }
}
