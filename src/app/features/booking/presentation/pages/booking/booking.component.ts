import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { BookingInteractor } from '@features/booking/core';
import { JourneyEntity } from '@features/booking/core/entities';
import { RouteNotAvailableException } from '../../../core/exceptions/route-not-available.exception';
import { BookingViewModel } from './booking.view-model';

const TIMER = 5000;

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.scss']
})
export class BookingComponent implements OnInit {

  public viewModel: BookingViewModel;
  public isLoading: boolean;
  public firstJourney: JourneyEntity;
  public secondJourney: JourneyEntity;
  public errorMessage: string | undefined;

  constructor(
    private formBuilder: FormBuilder,
    private bookingInteractor: BookingInteractor
  ) {
    this.viewModel = new BookingViewModel(this.formBuilder);
    this.isLoading = false;
  }

  public ngOnInit(): void {
    this.viewModel.initForm();
  }

  public get isFormValid(): boolean {
    return this.viewModel.form.valid
  }

  public async searchFlight() {
    console.log(this.viewModel.form.value)
    if (!this.viewModel.form.valid) return;

    try {
      this.isLoading = true;

      const flights = await this.bookingInteractor.getFlights();

      const [firstJourney, secondJourney] = await this.bookingInteractor.createJourney({
        ...this.viewModel.form.value,
        flights,
      });

      this.firstJourney = firstJourney;
      this.secondJourney = secondJourney;

      this.isLoading = false;
    } catch (error) {
      this.isLoading = false;

      if (error instanceof RouteNotAvailableException) {
        return this.handleErrors('No se encontraron rutas disponibles.');
      }
    }
  }

  private handleErrors(message: string) {
    this.errorMessage = message;

    setTimeout(() => {
      this.errorMessage = undefined;
    }, TIMER);
  }
}
