import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { EqualToValidator } from '@commons/validators';

export class BookingViewModel {
  public form: FormGroup;
  public origin: FormControl;
  public destination: FormControl;
  public oneWay: FormControl;

  constructor(private formBuilder: FormBuilder) {}

  public initForm() {
    this.origin = new FormControl('', [
      Validators.required,
      Validators.maxLength(3),
      Validators.minLength(3),
    ]);

    this.destination = new FormControl('', [
      Validators.required,
      Validators.maxLength(3),
      Validators.minLength(3),
    ]);

    this.oneWay = new FormControl(false);

    this.form = this.formBuilder.group({
      origin: this.origin,
      destination: this.destination,
      oneWay: this.oneWay,
    }, {
      validators: EqualToValidator,
    });
  }
}
