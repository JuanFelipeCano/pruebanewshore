import { CreateJourneyParam } from './params';
import { BookingRepository } from './repositories';
import { CreateJourneyUseCase } from './use-cases/create-journey.use-case';
import { GetFlightsUseCase } from './use-cases/get-flights.use-case';

export class BookingInteractor {
  protected getFlightsUseCase: GetFlightsUseCase;
  protected createJourneyUseCase: CreateJourneyUseCase;

  constructor(
    private repository: BookingRepository
  ) {
    this.getFlightsUseCase = new GetFlightsUseCase(this.repository);
    this.createJourneyUseCase = new CreateJourneyUseCase();
  }

  public getFlights() {
    return this.getFlightsUseCase.execute();
  }

  public createJourney(params: CreateJourneyParam) {
    return this.createJourneyUseCase.execute(params);
  }
}
