import { FlightEntity } from '../entities';

export abstract class BookingRepository {
  abstract getFlights(): Promise<FlightEntity[]>;
}
