interface TransportEntity {
  flightCarrier: string;
  flightNumber: string;
}

export interface FlightEntity {
  origin: string;
  destination: string;
  price: number;
  transport: TransportEntity;
}
