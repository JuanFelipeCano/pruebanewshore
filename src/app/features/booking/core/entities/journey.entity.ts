import { FlightEntity } from "./flight.entity";

export interface JourneyEntity {
  origin: string;
  destination: string;
  price: number;
  flights: FlightEntity[];
}
