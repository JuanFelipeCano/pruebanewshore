import { CoreExceptionBase } from '@commons/base';

export class RouteNotAvailableException extends CoreExceptionBase {}
