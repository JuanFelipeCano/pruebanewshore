import { UseCaseBase } from '@commons/base';
import { FlightEntity, JourneyEntity } from '../entities';
import { RouteNotAvailableException } from '../exceptions';
import { CreateJourneyParam } from '../params';

const getPrice = (flights: FlightEntity[]): number => {
  return flights.reduce((total, flight) => total + flight.price, 0);
}

export class CreateJourneyUseCase implements UseCaseBase<unknown, JourneyEntity[]> {

  constructor() {}

  public async execute(params: CreateJourneyParam): Promise<JourneyEntity[]> {
    try {
      const journey: JourneyEntity[] = [];
      journey.push(
        await this.getFlight(params)
      );

      if (!params.oneWay) {
        journey.push(
          await this.getFlight({
            ...params,
            origin: params.destination,
            destination: params.origin,
          })
        );
      }

      return journey;
    } catch (error) {
      throw error;
    }
  }

  private async getFlight(params: CreateJourneyParam): Promise<JourneyEntity> {
    const flights = await this.findFlights(params);

    if (flights.length === 0) {
      throw new RouteNotAvailableException(0);
    }

    const price = getPrice(flights);

    return {
      origin: params.origin,
      destination: params.destination,
      price,
      flights,
    };
  }

  private findFlights({ flights, origin, destination }: CreateJourneyParam): Promise<FlightEntity[]> {
    return new Promise((resolve) => {
      let queue: string[] = [origin];
      let visited: {[key: string]: boolean} = {};
      visited[origin] = true;
      let routes: {[key: string]: FlightEntity[]} = {};
      routes[origin] = [];

      while (queue.length > 0) {
        let current = queue.shift();
        for (let flight of flights) {
          if (flight.origin === current) {
            let route = routes[current].concat(flight);

            if (flight.destination === destination) {
              resolve(route);
            }

            if (!visited[flight.destination]) {
              visited[flight.destination] = true;
              queue.push(flight.destination);
              routes[flight.destination] = route;
            }
          }
        }
      }

      resolve([]);
    });
  }
}
