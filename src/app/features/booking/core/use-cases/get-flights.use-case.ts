import { UseCaseBase } from '@commons/base';
import { FlightEntity } from '../entities';
import { BookingRepository } from '../repositories';

export class GetFlightsUseCase implements UseCaseBase<unknown, FlightEntity[]> {

  constructor(
    private bookingRepository: BookingRepository
  ) {}

  public async execute(): Promise<FlightEntity[]> {
    try {
      return await this.bookingRepository.getFlights();
    } catch (error) {
      throw error;
    }
  }
}
