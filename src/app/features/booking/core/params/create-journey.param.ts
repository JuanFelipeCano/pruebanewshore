import { FlightEntity } from '../entities';

export interface CreateJourneyParam {
  origin: string;
  destination: string;
  oneWay: boolean;
  flights: FlightEntity[];
}
