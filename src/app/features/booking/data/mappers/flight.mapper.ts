import { MapperBase } from '@commons/base';
import { FlightResponseEntity } from '@api/entities';
import { FlightEntity } from '@features/booking/core/entities';

export class FlightMapper extends MapperBase<FlightResponseEntity, FlightEntity> {
  public mapTo(args: FlightResponseEntity): FlightEntity {
    return {
      origin: args.departureStation,
      destination: args.arrivalStation,
      price: args.price,
      transport: {
        flightCarrier: args.flightCarrier,
        flightNumber: args.flightNumber,
      },
    };
  }

  public override mapListTo(args: FlightResponseEntity[]): FlightEntity[] {
    return args.map((item) => this.mapTo(item));
  }
}
