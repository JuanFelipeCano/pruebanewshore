import { Injectable } from '@angular/core';
import { BookingService } from '@api/services';
import { BookingRepository } from '@features/booking/core/repositories';
import { FlightEntity } from '@features/booking/core/entities';
import { FlightMapper } from '../mappers/flight.mapper';

@Injectable()
export class BookingImplementationRepository implements BookingRepository {

  constructor(
    private bookingService: BookingService
  ) {}

  public async getFlights(): Promise<FlightEntity[]> {
    try {
      return new FlightMapper().mapListTo(
        await this.bookingService.getFlights()
      );
    } catch (error) {
      throw error;
    }
  }
}
