import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Currency, setCurrency } from '@commons/store';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {

  public currency$: Observable<Currency | any>;
  public readonly currencies = Currency;

  constructor(
    private store: Store<{ currency: Currency }>
  ) {
    this.currency$ = store.select('currency');
  }

  public setCurrency(currency: Currency) {
    this.store.dispatch(setCurrency({ currency }));
  }
}
