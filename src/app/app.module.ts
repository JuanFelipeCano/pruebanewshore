import { NgModule, Provider } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { StoreModule } from '@ngrx/store';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ApiModule } from '@api';
import { BookingRepository } from '@features/booking/core/repositories';
import { BookingImplementationRepository } from '@features/booking/data/repositories';
import { currencyReducer } from './commons/store';

const SERVICE_PROVIDERS: Array<Provider> = [
  {
    provide: BookingRepository,
    useClass: BookingImplementationRepository
  }
];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ApiModule,
    StoreModule.forRoot({ currency: currencyReducer }),
  ],
  providers: [SERVICE_PROVIDERS],
  bootstrap: [AppComponent]
})
export class AppModule { }
