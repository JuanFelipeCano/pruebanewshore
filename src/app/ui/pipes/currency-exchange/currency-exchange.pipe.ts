import { Pipe, PipeTransform } from '@angular/core';
import { CurrencyExchangeData } from '@commons/data';
import { Currency } from '@commons/store';

@Pipe({
  name: 'currencyExchange'
})
export class CurrencyExchangePipe implements PipeTransform {

  public transform(value: number, currency: Currency): number {
    if (!value || currency === Currency.USD) return value;

    return value * CurrencyExchangeData.rates[currency];
  }
}
