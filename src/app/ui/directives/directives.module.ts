import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ErrorMessageDirective } from './error-message/error-message.directive';
import { UppercaseDirective } from './uppercase/uppercase.directive';

@NgModule({
  declarations: [
    ErrorMessageDirective,
    UppercaseDirective,
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    ErrorMessageDirective,
    UppercaseDirective,
  ]
})
export class DirectivesModule { }
