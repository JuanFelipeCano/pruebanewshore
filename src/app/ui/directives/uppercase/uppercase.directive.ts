import { Directive, ElementRef, forwardRef, HostListener, Renderer2 } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
@Directive({
  selector: '[uppercase]',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => UppercaseDirective),
      multi: true,
    }
  ]
})
export class UppercaseDirective implements ControlValueAccessor {

  private onChange: (args: any) => void;
  private onTouched: () => void;

  constructor(
    private elementRef: ElementRef<HTMLElement>,
    private renderer: Renderer2
  ) { }

  @HostListener('input', ['$event'])
  public keyUp(event: any) {
    const value = event.target.value.toUpperCase();
    this.renderer.setProperty(this.elementRef.nativeElement, 'value', value);

    this.onChange(value);
    event.preventDefault();
  }

  @HostListener('blur')
  public onBlur() {
    this.onTouched();
  }

  public writeValue(value: any): void {
    this.renderer.setProperty(this.elementRef.nativeElement, 'value', value);
  }

  public registerOnChange(fn: (args: any) => void): void {
    this.onChange = fn;
  }

  public registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }

  public setDisabledState(isDisabled: boolean): void {
    this.renderer.setProperty(this.elementRef.nativeElement, 'disabled', isDisabled);
  }
}
