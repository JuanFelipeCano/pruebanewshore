import { Directive, ElementRef, HostListener, Input, Renderer2 } from '@angular/core';
import { ErrorMessageEntity } from './error-message.entity';

const TIMER = 100;

@Directive({
  selector: '[errorMessage]'
})
export class ErrorMessageDirective {

  @Input('errorMessage')
  public config: ErrorMessageEntity;

  private spanError: HTMLSpanElement | any;

  constructor(
    private elementRef: ElementRef<HTMLElement>,
    private renderer: Renderer2
  ) { }

  @HostListener('input')
  public change() {
    setTimeout(() => {
      if (this.config.control.status === 'INVALID') {
        for (const prop in this.config.control.errors) {
          this.removeErrorMessage();
          if (!this.config.errorMessages[prop]) return;

          this.addErrorMessage(this.config.errorMessages[prop]);
        }

        return;
      }

      this.removeErrorMessage();
    }, TIMER);
  }

  private addErrorMessage(errorMessage: string) {
    this.removeErrorMessage();

    this.spanError = this.renderer.createElement('span');
    this.spanError.className = 'text-danger';
    this.spanError.innerHTML = errorMessage;
    this.renderer.appendChild(this.elementRef.nativeElement.parentElement, this.spanError);
  }

  private removeErrorMessage() {
    if (this.spanError) {
      this.renderer.removeChild(
        this.elementRef.nativeElement.parentElement,
        this.spanError
      );

      this.spanError = undefined;
    }
  }

}
