import { AbstractControl } from '@angular/forms';

export interface ErrorMessageEntity {
  control: AbstractControl,
  errorMessages: ErrorMessage,
}

export type ErrorMessage = Record<string, string>;
