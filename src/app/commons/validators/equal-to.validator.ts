import { ValidationErrors, FormGroup  } from '@angular/forms';

export const EqualToValidator = (
  control: FormGroup
): ValidationErrors | null => {
  const origin = control.get('origin');
  const destination = control.get('destination');

  return (
    (origin?.value || destination?.value)
    && origin?.value === destination?.value
  )
    ? { equalTo: true }
    : null;
}
