import { createReducer, on } from '@ngrx/store';
import { Currency } from './currency';
import { setCurrency } from './currency.actions';

export const initialState = { currency: Currency.USD };

export const currencyReducer = createReducer(
  initialState,
  on(setCurrency, (state, payload) => ({
      ...state,
      currency: payload.currency,
    })
  )
);
