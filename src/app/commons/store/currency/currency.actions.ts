import { createAction, props } from '@ngrx/store';
import { Currency } from './currency';

export const setCurrency = createAction(
  '[Currency] setCurrency',
  props<{ currency: Currency }>()
);
