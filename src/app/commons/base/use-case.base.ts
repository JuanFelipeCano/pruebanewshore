export interface UseCaseBase<Request, Response> {
  execute(params: Request): Promise<Response> | Response;
  execute(...params: unknown[]): Promise<Response> | Response;
}
