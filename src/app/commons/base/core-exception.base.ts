export class CoreExceptionBase<T extends unknown = unknown> extends Error {

  constructor(
    public error: any = null,
    public readonly code?: string,
    public readonly data?: T
  ) {
    super((error as CoreExceptionBase)?.message ?? (error as string));

    this.code = this.code ?? error.code;
    this.data = this.data ?? (error.data as T);

    Object.setPrototypeOf(this, new.target.prototype);
  }
}
