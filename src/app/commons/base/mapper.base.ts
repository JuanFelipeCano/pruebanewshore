export abstract class MapperBase<O, T> {
  abstract mapTo(args: O): T;

  public mapListTo(args: O[]): T[] {
    return args.map((model) => this.mapTo(model)) as T[];
  }
}
