import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { ApiModule, API_URL } from '../api.module';
import { lastValueFrom } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: ApiModule
})
export class HttpService {

  constructor(
    private http: HttpClient,
    @Inject(API_URL) public ApiUrl: string
  ) { }

  public async get<T>(url: string): Promise<T> {
    const endpoint = this.ApiUrl + url;

    const response = this.http.get<T>(endpoint, this.getHttpOptions()).pipe(
      map(response => response),
    );

    return await lastValueFrom(response);
  }

  private getHttpOptions() {
    return {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };
  }
}
