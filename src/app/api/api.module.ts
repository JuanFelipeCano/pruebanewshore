import { InjectionToken, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ApiInterceptor } from './http-service/api-interceptor/api.interceptor';

export const API_URL = new InjectionToken<string>('');
const URL = 'https://recruiting-api.newshore.es/api/flights/2';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule,
  ],
  exports: [
    HttpClientModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiInterceptor,
      multi: true
    },
    {
      provide: API_URL,
      useValue: URL,
    }
  ]
})
export class ApiModule { }
