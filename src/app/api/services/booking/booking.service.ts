import { Injectable } from '@angular/core';
import { ApiModule } from '../../api.module';
import { FlightResponseEntity } from '../../entities/flight-response.entity';
import { HttpService } from '../../http-service/http.service';

@Injectable({
  providedIn: ApiModule
})
export class BookingService {

  constructor(
    private httpService: HttpService
  ) { }

  public getFlights() {
    return this.httpService.get<FlightResponseEntity[]>('');
  }
}
